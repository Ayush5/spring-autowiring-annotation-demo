package com.esewa.springdemoAutowiringByContructorDemo;

import com.esewa.springdemoAutowiringByPropertiesDemo.Pancard;
import org.springframework.beans.factory.annotation.Autowired;

public class Employee {

    public int employeeID;
    public String name;
    public Pancard pancard;

@Autowired
    public Employee(int employeeID, String name, Pancard pancard) {
        this.employeeID = employeeID;
        this.name = name;
        this.pancard = pancard;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public String getName() {
        return name;
    }

    public Pancard getPancard() {
        return pancard;
    }

}
