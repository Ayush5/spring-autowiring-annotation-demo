package com.esewa.springdemoAutowiringByPropertiesDemo;

public class Pancard {
    public String panNumber;
    public String pancardHolderName;

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getPancardHolderName() {
        return pancardHolderName;
    }

    public void setPancardHolderName(String pancardHolderName) {
        this.pancardHolderName = pancardHolderName;
    }

    @Override
    public String toString() {
        return "Pancard{" +
                "panNumber='" + panNumber + '\'' +
                ", pancardHolderName='" + pancardHolderName + '\'' +
                '}';
    }
}
